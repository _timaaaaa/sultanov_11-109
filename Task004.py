def distance_levenshtein(a, b):
    if not a: return len(b)
    if not b: return len(a)
    return min(distance_levenshtein(a[1:], b[1:]) + (a[0] != b[0]),
               distance_levenshtein(a[1:], b) + 1,
               distance_levenshtein(a, b[1:]) + 1)


print(distance_levenshtein("qaedwstgrf", "qawsedrftg"))